# ---[ Make variables - pass these to make ]------------------------------------
CFLAGS = -std=c99 -g -Werror -Wall
PREFIX = ./build
DEBUG = 0


# ---[ Directories ]------------------------------------------------------------
OBJDIR = $(PREFIX)/obj
LIBDIR = $(PREFIX)/lib
INCDIR = $(PREFIX)/include
BINDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/man


# ---[ Conditional stuff ]------------------------------------------------------
ifeq ($(DEBUG), 0)
CFLAGS += -O2
else
CFLAGS += -O0
endif


# ---[ Phony targets list ]-----------------------------------------------------
.PHONY = help clean bin man lib inc


# ---[ Main targets ]-----------------------------------------------------------
help:
	@echo "Usage: make [<targets>]"
	@echo
	@echo "The targets are one or more of the following:"
	@echo "  - all    Build all binaries, headers, and documentation"
	@echo "  - bin    Build binaries only"
	@echo "  - inc    Build header files only"
	@echo "  - lib    Build static library only"
	@echo "  - man    Build manual pages"
	@echo "  - clean  Remove all build products"
	@echo "  - help   Show this help message"
	@echo
	@echo "The following variables can be passed to Make:"
	@echo "  - PREFIX  Location for the build products"
	@echo

all: bin inc lib man


# ---[ Product targets ]--------------------------------------------------------
bin: $(BINDIR)/roll

inc: $(INCDIR)/roll.h

lib: $(LIBDIR)/libroll.a

man: $(MANDIR)/man/man1/roll.1


# ---[ Source targets ]---------------------------------------------------------
$(OBJDIR)/%.o: src/%.c src/%.h
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) -c -o $@ $<

$(OBJDIR)/%.o: src/%.c
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) -c -o $@ $^

$(INCDIR)/%.h: src/lib%.h
	@mkdir -p $(INCDIR)
	@cp $^ $@

$(LIBDIR)/libroll.a: $(OBJDIR)/libroll.o
	@mkdir -p $(LIBDIR)
	@ar -rsc $@ $^

$(BINDIR)/roll: $(OBJDIR)/roll.o $(OBJDIR)/libroll.o
	@mkdir -p $(BINDIR)
	@$(CC) $(CFLAGS) -o $@ $^


# ---[ Man targets ]------------------------------------------------------------
$(MANDIR)/man/man1/roll.1: ./man/man1/roll.1
	@mkdir -p $(MANDIR)/man/man1
	@cp ./man/man1/roll.1 $(MANDIR)/man/man1/roll.1


# ---[ Phony targets ]----------------------------------------------------------
clean:
	@rm -rf $(PREFIX)/
