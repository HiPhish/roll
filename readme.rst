.. default-role:: code

######################################
`roll` - Roll dice on the command-line
######################################

Have you ever found yourself trying to reach maximum nerdage by playing
*Dungeons & Dragons* over a teletype only to find yourself lacking a way of
rolling dice and piping the result into your MUD? Me neither, but I wrote one
anyway, just in case.  I guess you could also use it for diceware passphrase
generation, but seriously, who does that?



Building
########

All you need is a `make` with support for conditions (e.g. GNU make), a C99
standard-compliant compiler and a POSIX1.2001 compliant implementation of the
standard C library.  Then run

.. code:: sh

   make [bin header lib man]

in the root of the project to build the parts you need.  If no arguments are
given everything is built.  The compiled output can be found in the `build`
directory, or in a custom directory if the `PREFIX` Variable has been defined
(see below).

There is no installation script because the locations are different for
different systems.  Just move the contents of the built directories where it is
appropriate for you.  Or even better, have your package manager do it for your.

Makefile variables
==================

The following makefile variables can be passed to `make` when building:

========  =============  =======================================================
Variable  Default        Meaning
========  =============  =======================================================
`CC`      cc             Compiler
`CFLAGS`  -std=c99 -g    Compiler flags
          -Werror -Wall
`PREFIX`  ./build        Root build directory, all other build directories are
                         relative to this.
`DEBUG`   0              Set to non-zero to disable compiler optimisations
========  =============  =======================================================



Using
#####

`roll` is both a standalone application as well as a C library.  As always the
manpage is the authoritative documentation, consider this the TL;DR version.

Binary
======

Run the binary with this syntax:

.. code:: sh

   roll [options] rds [... [options] rds]

What this means is that we specify our roll as the number of rolls, followed
immediately by a `d`, followed immediately by the number of sides per die.  To
roll five six-sided dice we would give the argument `5d6`.

The options are `--seed` (or `-s`) followed by a number, `--no-seed` (or `-S`)
(the default) and `--delimiter` (or `-d`) followed by a delimiter string (a
single space by default).  The delimiter is not printed after the last roll.

The seed is optional.  If no seed is given a seed is generated according to the
implementation of `srandomdev()`, which is supposed to be cryptographically
suitable.  Specifying the same seed guarantees the same results.

One can specify several rolls in succession, their results will be printed in
that order.  A seed will always apply to all subsequent rolls, even when other
dice are used.

.. code:: sh

   roll 3d6 --seed 7 2d8 3d4 --no-seed 2d20

In the above example the first three numbers are printed with unspecified seed,
the next five with a seed of seven and the last two are printed with
unspecified seed again.  The argument `--no-seed` resets back the default
behaviour.

Library
=======

The library provides a function for generating uniformly distributed numbers in
the range form `1` to `sides` (both inclusive).  The parameter `sides` must not
exceed the maximum allowed number of sides as specified in the header file.

.. code:: cpp

   #define ROLLS  5
   #define SIDES  6

   int random_number[ROLLS];
   for (int i = 0; i < 5; ++i) {
      random_number[i] = roll_die(SIDES);
   }

The function will fail an assertion if your number of sides is too large, so
check your value first.  This is not an oversight, it's intentional.



License
#######

The MIT License (MIT)

Copyright (c) 2015 HiPhish

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

