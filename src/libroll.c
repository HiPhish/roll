/** @file libroll.c
 *
 *  Implementation of the `roll` library.
 */

#include <stdlib.h>
#include <assert.h>

#include "libroll.h"

int roll_die(int sides) {
	assert(sides <= ROLL_SIDES_MAX);
	if (sides == ROLL_SIDES_MAX) {
		return random() + 1;
	}

	long end = (ROLL_SIDES_MAX/ sides) * sides;
	assert(end > 0);

	long rand;
	while ((rand = random()) >= end) {;}

	return (rand % sides) + 1;
}

