/** @file libroll.h
 *
 *  Public interface to the `roll` library.
 */

/** Maximum number of sides for a die.
 *
 * 	This is one more than the largest number the RNG can generate. That's
 * 	because a die counts from one instead of zero.
 */
#define ROLL_SIDES_MAX  0xFFFFFFFF

/** Rolls a uniform die and returns the result.
 *
 *  @param sides  How many dies the die has.
 *
 *  @return  Integer between `1` and `sides` (both inclusive).
 *
 *  @pre  The caller is responsible for making sure the see was set.
 *  @pre  The number of sides must be less or equal to `ROLL_SIDES_MAX`. This
 *        is mandatory, otherwise the assertion will fail and the program will
 *        crash.
 *
 *  The functions uses the POSIX.1-2001 function `random()` to generate the
 *  roll, and the user is responsible for setting an appropriate seed first.
 *
 *  To ensure uniformity the RNG might have to run more than once, discarding
 *  unsuitable numbers. Due to the randomness it's impossible to predict the
 *  number of attempts, but even in the worst case there is only a 50% chance
 *  of having to discard a number.
 */
int roll_die(int sides);

