/** @file roll.c
 *
 *  Main source file for `roll` command-line application.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libroll.h"

/** Possible return values of the application. */
enum return_values {
	R_SUCCES,         /**< Successful termination.    */
	R_ARGS_COUNT,     /**< Wrong amount of arguments. */
	R_ARGS_INV,       /**< Invalid arguments.         */
	R_ARGS_DUP,       /**< Duplicate argument.        */
	R_ARGS_ORDER,     /**< Order of arguments wrong.  */
	R_SIDE_OVERFLOW,  /**< Sides too large for RNG.   */
};

/** Process arguments passed to the application.
 *
 *  @param argc  Number of arguments.
 *  @param argv  Array of argument strings.
 *  @param sett  Pointer to settings object to mutate.
 *  
 *  @return  Error code, same as for the application.
 */
static int handle_args(int argc, char **argv);

/** Print usage instructions to standard error.
 *
 *  @param error  Error code, maps to an error message.
 */
static void print_usage(int error);

/** Get a suitable value for seeding. */
static unsigned int get_random_seed();


int main(int argc, char **argv) {
	int return_val = R_SUCCES;

	if ((return_val = handle_args(argc, argv))) {
		goto failure;
	}

	/* Print a newline */
	puts("");

	return return_val;

failure:
	print_usage(return_val);
	return return_val;
}

static int handle_args(int argc, char **argv) {
	/* Make sure we have the right number of arguments */
	if (argc == 1) {
		return R_ARGS_COUNT;
	}

	/* Character to delimit results */
	char *delimiter = " ";

	/* Set a safe seed */
	srandom(get_random_seed());

	/* Loop over the arguments */
	for (int i = 1; i < argc; ++i) {
		char *arg = argv[i];  /* The current option to handle     */
		char *str = NULL;     /* Use of second argument of strtol */

		/* Argument is an option */
		if (arg[0] == '-') {
			/* Option sets the seed */
			if (strcmp(arg, "--seed") == 0 || strcmp(arg, "-s") == 0) {
				int seed;  /* Value to seed */

				/* There must be an argument after this */
				if (++i == argc) {
					return R_ARGS_COUNT;
				}
				arg = argv[i]; /* Shift to next argument */

				seed = (int)strtol(arg, &str, 10);
				if (*str != '\0') {
					return R_ARGS_INV;
				}

				srandom(seed);
			} else if (strcmp(arg, "--no-seed") == 0 || strcmp(arg, "-S") == 0) {
				srandom(get_random_seed());
			} else if (strcmp(arg, "--delimiter") == 0 || (strcmp(arg, "-d") == 0)) {
				/* There must be an argument after this */
				if (++i == argc) {
					return R_ARGS_COUNT;
				}
				arg = argv[i]; /* Shift to next argument */
				delimiter = arg;
			} else {
				/* Unkown option */
				return R_ARGS_INV;
			}
		} else {
			/* The currently specified die settings */
			int rolls = 0;
			int sides = 0;

			int  j = 0;  /* Index into the argument string */
			int  d = 0;  /* Position of the 'd' character  */

			/* Loop over the argument string */
			for (char c = arg[0]; c != '\0'; c = arg[++j]) {
				/* Only `0` - `1`, `d` and `D` are allowed. */
				if ((c < '0' || c > '9') && c != 'd' && c != 'D') {
					return R_ARGS_INV;
				}
				/* The `d` marks the split */
				if (c == 'd' || c == 'D') {
					/* There may be only one `d` per die */
					if (d != 0 || arg[j+1] == '\0') {
						return R_ARGS_INV;
					}
					d = j;  /* Set the index of the `d` */
				}
			}

			char rolls_s[  d+1];  /* String of rolls. */
			char sides_s[j-d+1];  /* String of sides. */

			/* The `d` may not be the first character */
			if (d == 0) {
				return R_ARGS_INV;
			}

			/* Split `arg` into the two number string */
			sprintf(rolls_s, "%.*s",   d, arg      );
			sprintf(sides_s, "%.*s", j-d, arg + d+1);

			/* We can be certain this will be successful */
			rolls = atoi(rolls_s);
			sides = atoi(sides_s);

			/* Check for too many sides */
			if (sides > ROLL_SIDES_MAX) {
				return R_SIDE_OVERFLOW;
			}

			for (int k = 0; k < rolls; ++k) {
				printf("%i", roll_die(sides));

				/* Skip the last roll of the last die */
				if (!(i == argc-1 && k == rolls-1)) {
					printf("%s", delimiter);
				}
			}
		}
	}

	return R_SUCCES;
}

static unsigned int get_random_seed() {
	unsigned seed = 0;
	FILE *random = fopen("/dev/random", "rb");
	fread(&seed, sizeof seed, 1, random);
	fclose(random);
	return seed;
}

static void print_usage(int error) {
	/* Map error codes to error messages */
	char *error_msg[] = {
		[R_SUCCES    ]    = "",
		[R_ARGS_COUNT]    = "Error: Wrong number of arguments.\n",
		[R_ARGS_INV  ]    = "Error: Invalid arguments.\n",
		[R_ARGS_DUP  ]    = "Error: Duplicate arguments.\n",
		[R_ARGS_ORDER]    = "Error: Must specify seed before dice.\n",
		[R_SIDE_OVERFLOW] = "Error: Sides must not be larger than '2^31 - 1'.\n",
	};

	char *usage_string = "Usage:  roll <rolls>d<sides>\n";

	fputs(error_msg[error], stderr);
	fputs(usage_string    , stderr);
}

